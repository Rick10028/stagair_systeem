<?php
session_start();
require './ConnectionDB/db.php';

$mysqli =  new mysqli('localhost', 'root', '', 'multi_login') or die(mysqli_error($mysqli));

global $errors;
$errors = array();

if (isset($_POST['login-btn'])) {

    $username = $_POST['username'];
    $password = $_POST['password'];

    if (empty($username)) {
        $errors['username'] = "Email verplicht";
    }
    if (empty($password)) {
        $errors['password'] = "Password verplicht";
    }

    if(count($errors) === 0) {
      $sql = "SELECT * FROM users WHERE email=? LIMIT 1";
      $stmt = $conn->prepare($sql);
      $stmt->bind_param('s', $username);
      $stmt->execute();
      $result = $stmt->get_result();
      $user = $result->fetch_assoc();

      if (password_verify($password, $user['password'])) {
        $_SESSION['id'] = $user['id'];
        $_SESSION['username'] = $user['username'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['rol'] = $user['rol'];
        $_SESSION['klas_id'] = $user['klas_id'];
        $_SESSION['supervisor_id'] = $user['supervisor_id'];
        $_SESSION['alert-class'] = "alert-succes";
        header('location: index.php');
        exit();
      }else {
        $errors['login_fail'] = "Verkeerde email / wachtwoord combinatie. Of account nog niet geverifieerd!";
      }
    }
}

if (isset($_GET['logout'])) {
    destroySession();
    redirectToIndex();
}
if (isset($_POST['signup-btn'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $passwordConf = $_POST['passwordConf'];
    handleSignup($username , $email, $password, $passwordConf);
  }
                                     /// FUNCTIONS: ///
function handleSignup($username , $email, $password, $passwordConf) {
    global $errors;
    //Give errors when values are incorrect.
    if (empty($username)) {
        $errors['username'] = "Gebruikersnaam verplicht";
    }
    if (empty($email)) {
        $errors['email'] = "Email verplicht";
    }
    if (empty($password)) {
        $errors['password'] = "Password verplicht";
    }
    if ($password !== $passwordConf) {
        $errors['password'] = "De wachtwoorden komen niet overeen";
    }

    if (count($errors) === 0) if (userExists($email)) $errors['userExists']="Dit email  is al in gebruik. ";

    $password = password_hash($password, PASSWORD_DEFAULT);

    if (count($errors) === 0) $id = insertUser($username , $email, $password, $token, $auth);

    if (count($errors) === 0) makeSession($id, $email, $rol, $username, $klas_id, $supervisor_id);

    if (count($errors) === 0) redirectToVerifyPage();
}

//returns true if the user exists in the database, otherwise returns false
function userExists($email) {
    global $conn;
    $emailQuery = "SELECT * FROM users WHERE email=? LIMIT 1";
    $stmt = $conn->prepare($emailQuery);
    $stmt->bind_param('s', $email);
    $stmt->execute();
    $result = $stmt->get_result();
    $userCount = $result->num_rows;
    $stmt->close();

    if ($userCount > 0) {
        return true;
    }
    else {
        return false;
    }
}

//inserts a user into the database with the given parameters
function insertUser($username, $email, $password) {
    global $conn;
    $verified = false;
    $sql = "INSERT INTO users (username, email, password) VALUES(?, ?, ?)";
    $stmt = $conn->prepare($sql);
    if ( false === $stmt ) {
	 die('prepare() failed: ' . htmlspecialchars($stmt->error));
    }
    $stmt->bind_param('sss', $username, $email, $password);
    if ( false === $stmt) {
         die('bind_param() failed: ' . htmlspecialchars($stmt->error));
    }
   if ($stmt->execute()) {
        $user_id = $conn->insert_id;
        return $user_id;

        exit();
    } else {
        die("Oeps! Er is iets mis gegaan tijdens het aanmaken van jouw account!" . $stmt->error);
    }
}

//creates a session when user is logged in
function makeSession($id, $email, $rol, $username, $klas_id, $supervisor_id){
    $_SESSION['id'] = $id;
    $_SESSION['email'] = $email;
    $_SESSION['rol'] = $rol;
    $_SESSION['username'] = $username;
    $_SESSION['klas_id'] = $klas_id;
    $_SESSION['supervisor_id'] = $supervisor_id;
}

//redirect user to the login page
function RedirectToLogin() {
    header('location: login.php');
    exit();
}

//redirect user to the verify account page
function redirectToVerifyPage() {
    header('location: login.php');
    exit();
}

//redirect user to index page
function redirectToIndex() {
    header('location: index.php');
    exit();
}

//destroys session when user logging out
function destroySession(){
    session_destroy();
    unset($_SESSION['id']);
    unset($_SESSION['username']);
    unset($_SESSION['email']);
    unset($_SESSION['rol']);
    unset($_SESSION['klas_id']);
    unset($_SESSION['supervisor_id']);
}

?>

