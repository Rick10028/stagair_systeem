<!DOCTYPE HTML>
<html>
<?php
    include('Controllers/AuthControllers.php');
   
    require_once 'includes/header.php';
    require_once 'includes/navigation.php';
    
    if(isset($_POST['edit_user'])) {
      $id = $_POST['id'];
      $username = $_POST['username'];
      $email = $_POST['email'];
      $password = $_POST['password'];
      $company = $_POST['company'];
      $rol = $_POST['rol'];

      if (editUser($id, $username, $email, $password, $company, $rol)){?>
          <div class="alert alert-success">
          <strong>Success!</strong><?php echo " Gebruiker veranderd";?>
          </div>
          <?php
      }
  } else {
      $id=$_GET['id'];
      if(!isMyOwnProfile($id) && !iAmAdmin()) die('Je mag hier niet komen');
  }
  // Returns true if succes, return false if user gives wrong input //
  // dies on error //
  function editUser($id, $username, $email, $password, $company, $rol) {
  
      global $conn;
      $sql = "UPDATE users SET username=?, email=?, password=?, company=?, rol=? WHERE id='$id'";
      $stmt = $conn->prepare($sql);
      $password = password_hash($password, PASSWORD_DEFAULT);
      if($stmt === false) {
          die('prepare failed:'.htmlspecialchars($stmt->error));
      }
      $stmt->bind_param('sssss', $username, $email, $password, $company, $rol);
      if($stmt->execute()) {
          return true;
      }
      if($stmt === false) {
          die('prepare() failed:'. htmlspecialchars($stmt->error));
      }
  }
  
  function isMyOwnProfile($id){
      if($_SESSION['id'] == $id) return true;
      else return false;
  }
  function iAmAdmin(){
      if($_SESSION['rol']=='Admin') return true;
      else return false;
  }

  $sql="SELECT id, username, email, company, supervisor_id FROM users WHERE id='$id'";

  if($stmt = $mysqli->prepare($sql)){
      if(!$stmt->execute()){
          echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
      }else{
          $stmt->bind_result($id, $username, $email, $company, $supervisorId);
      }
      $stmt->close();
  }else{
      echo 'er zit een fout in de query:'.$mysqli->error;
  }
  $result = $conn->query($sql);
  
  if($result-> num_rows>0){
      while($row = $result-> fetch_assoc()){
        $supervisorId = $row['supervisor_id'];
?>
                  <div class="col-lg-12 mx-auto mt-5 text-white text-center">
                      <h1 class="display-4">Verander Gebruiker </h1>
                      </p>
                  </div>

<div class="container mt-5">
    <div class="bg-white rounded-lg p-5 shadow">
        <div>
            <a href="admin.php" class="Terug"><span>Terug</span></a>
        </div>
        <div class="row">
            <div class="col-lg-4 pb-5">
                <!-- Account Sidebar-->
                <div class="author-card pb-3">
                    <div class="author-card-cover" style="background-image: url(https://demo.createx.studio/createx-html/img/widgets/author/cover.jpg);"><a class="btn btn-style-1 btn-white btn-sm" href="#" data-toggle="tooltip" title=""><i class="fa fa-award text-md"></i>&nbsp;Aanwezig</a></div>
                    <div class="author-card-profile">
                        <div class="author-card-avatar"><img src="images/profiel.jpg" alt="<?php echo $row['username'] ?>"">
                        </div>
                        <div class="author-card-details">
                            <h5 class="author-card-name text-lg"><?php echo $row['username'] ?></h5><span class="author-card-position">Joined February 06, 2017</span>
                        </div>
                    </div>
                </div>
                <div class="wizard">
                    <!--This is a loop for all supervisors, shows all interns-->
                    <?php
                    }
                    }
                    $sql="SELECT id, username FROM users WHERE supervisor_id = '$id'";

                    if($stmt = $mysqli->prepare($sql)){
                        if(!$stmt->execute()){
                            echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
                        }else{
                            $stmt->bind_result( $id, $username);
                        }
                        $stmt->close();
                    }else{
                        echo 'er zit een fout in de query:'.$mysqli->error;
                    }
                    $result = $conn->query($sql);

                    if($result-> num_rows>0){?>
                        <nav class="list-group list-group-flush">
                            <a class="list-group-item active" href="#"><i class="fe-icon-user text-muted"></i>Stagiares</a>
                        </nav><?php
                        while($row = $result-> fetch_assoc()){

                            ?>
                            <a class="list-group-item" href="/show_profile.php?id=<?=$row['id']?>">
                                <div class="d-flex justify-content-between align-items-center">
<div><i class="fe-icon-heart mr-1 text-muted"></i>
                                        <div class="d-inline-block font-weight-medium text-uppercase"><?php echo $row['username'] ?></div>
                                    </div>
                                </div>
                            </a>

                            <!--This is a loop for all interns, shows their supervisor-->
                            <?php
                        }
                    }
                    $sql="SELECT id, username FROM users WHERE '$supervisorId' = id";

                    if($stmt = $mysqli->prepare($sql)){
                        if(!$stmt->execute()){
                            echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
                        }else{
                            $stmt->bind_result( $id, $username);
                        }
                        $stmt->close();
                    }else{
                        echo 'er zit een fout in de query:'.$mysqli->error;
                    }
                    $result = $conn->query($sql);

                    if($result-> num_rows>0){?>
                        <nav class="list-group list-group-flush">
                            <a class="list-group-item active" href="#"><i class="fe-icon-user text-muted"></i>Stage begeleider</a>
                        </nav><?php
                        while($row = $result-> fetch_assoc()){

                            ?>
                            <a class="list-group-item"  href="/show_profile.php?id=<?=$row['id']?>">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div><i class="fe-icon-heart mr-1 text-muted"></i>
                                        <div class="d-inline-block font-weight-medium text-uppercase"><?php echo $row['username'] ?></div>
                                    </div>
                                </div>
                            </a>
                                                              <?php
                                                          }
                                                      }
                                                      $sql="SELECT id, username, email, company, rol FROM users WHERE id='$id'";
  
                                                      if($stmt = $mysqli->prepare($sql)){
                                                          if(!$stmt->execute()){
                                                              echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
                                                          }else{
                                                              $stmt->bind_result($id, $username, $email, $company, $rol);
                                                          }
                                                          $stmt->close();
                                                      }else{
                                                          echo 'er zit een fout in de query:'.$mysqli->error;
                                                      }
                                                      $result = $conn->query($sql);
  
                                                      if($result-> num_rows>0){
                                                          while($row = $result-> fetch_assoc()){
                                                  ?>
                                  </div>
                              </div>
                              <!-- Profile Settings-->
                              <div class="col-lg-8 pb-5">
                                  <form class="row" action="" method="post">
                                      <input type="hidden" name="id" value="<?php echo $id; ?>">
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label for="account-fn">username</label>
                                              <input class="form-control" type="text" name="username" value="<?php echo $row['username'] ?>" required="">
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label for="account-company">Bedrijf</label>
                                              <input class="form-control" type="text" name="company" value="<?php echo $row['company'] ?>">
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label for="account-email">E-mail Address</label>
                                              <input class="form-control" type="email" name="email" value="<?php echo $row['email'] ?>">
                                          </div>
                                      </div>
                                      <div class="col-md-6">
                                          <div class="form-group">
                                              <label for="account-pass">New Password</label>
                                              <input class="form-control" type="password" name="password" value="">
                                          </div>
                                      </div>
                                      <div class="form-group">
                                            <label for="">Selecteer Rol:</label>
                                            <?php
                                            $sql = "SELECT rol FROM users WHERE id = $id";
                                            if($stmt = $mysqli->prepare($sql)){
                                                if(!$stmt->execute()){
                                                echo 'uitvoeren vna query mislukt'.$stmt->close().'in query'.$sql;
                                                }else{
                                                $stmt->bind_result($rol);
                                                }
                                                $stmt->close();
                                            }else{
                                                echo 'er zit een fout in de query:'.$mysqli->error;
                                            }
                                            $result = mysqli_query($conn, $sql);
                                            $options = "";

                                            while($row = mysqli_fetch_array($result)){
                                                $options = $options."<option>User</option><option>Leraar</option><option>Stage</option><option>Admin</option>";
                                            }
                                            ?>
                                            <select  name="rol">
                                            <?php echo $options;?>
                                            </select>
                                        </div>
                                      <div class="col-12">
                                          <hr class="mt-2 mb-3">
                                          <div class="d-flex flex-wrap justify-content-between align-items-center">
                                              <div class="custom-control custom-checkbox d-block">
                                                  <input class="custom-control-input" type="checkbox" id="subscribe_me" checked="">
                                              </div>
                                              <button class="btn btn-style-1 btn-primary" type="submit" name="edit_user"data-toast="" data-toast-position="topRight" data-toast-type="success" data-toast-icon="fe-icon-check-circle" data-toast-title="Success!" data-toast-message="Your profile updated successfuly.">Update Profile</button>
                                          </div>
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
          <?php
         }
          }
require_once 'includes/footer.php';

      ?>
      </div>
</html>
