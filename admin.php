<!DOCTYPE HTML>
<html>
<?php
    include('Controllers/AuthControllers.php');
   
    require_once 'includes/header.php';
    require_once 'includes/navigation.php';
    
    $username = $_SESSION['username'];
$c = $conn->prepare("SELECT * FROM users WHERE username=?");
$c->bind_param("s", $username);
$c->execute();
$result = $c->get_result();
while($row = $result->fetch_assoc()){
    if($row['rol']!= "Admin")
{
    header('location: index.php');
}
}
    
?>
<div class="container py-5">
    <div class="col-lg-12 mx-auto mb-5 text-white text-center">
        <h1 class="display-4">Alle gebruikers</h1>
    </div>
    <div class="bg-white rounded-lg p-3 shadow">
        <div class="container-xl">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-5">
                                <h2>User <b>Management</b></h2>
                            </div>
                            <div class="col-sm-7">
                                <a href="adduser.php" class="btn btn-secondary"><i class="fas fa-plus-square"></i> <span>Add New User</span></a>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Date Created</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>


     <?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "multi_login";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

$sql = "SELECT id,username,email,rol FROM users";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {?>
                                <tr>
                                    <td><?php echo $row['id'] ?></td>
                                    <td><?php echo $row['username'] ?></a></td>
                                    <td>04/10/2013</td>
                                    <td><?php echo $row['rol'] ?></td>
                                    <td><span class="status text-success">&bull;</span> Active</td>
                                    <td>
                                       
                                          <?php     echo    '<a href="edit-user.php?id='.$row['id'].'" class="settings" title="Settings" data-toggle="tooltip"><i class="fas fa-cog"></i></a>'; ?>
                               <?php     echo    '<a href="delete.php?id='.$row['id'].'" class="delete" title="deleteUser" data-toggle="tooltip"><i class="fas fa-trash-alt"></i></a>'; ?>
                                    </td>
                                </tr>

                <?php
  }
} else {
  echo "geen users in database     ";
}
$conn->close();
?>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

    require_once 'includes/footer.php';
    
?>

</html>
