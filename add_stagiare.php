<?php

include('Controllers/AuthControllers.php');


if($_SESSION['username']==NULL){
    header('location: login.php');
}

require_once 'includes/header.php';
require_once 'includes/navigation.php';

if(isset($_POST['edit_student'])) {
    $id = $_POST['id'];
    $supervisor_id = $_SESSION['id'];

    if (editUser($id, $supervisor_id)){?>
        <div class="alert alert-success">
            <strong>Success!</strong><?php echo " Stagiaire toegevoegd";?>
        </div>
        <?php
    }
}
// Returns true if succes, return false if user gives wrong input //
// dies on error //
$supervisor_id = $_SESSION['id'];
function editUser($id, $supervisor_id) {
    global $conn;
    $sql = "UPDATE users SET supervisor_id=? WHERE id='$id'";
    $stmt = $conn->prepare($sql);
    if($stmt === false) {
        die('prepare failed:'.htmlspecialchars($stmt->error));
    }
    $stmt->bind_param('s',$supervisor_id);
    if($stmt->execute()) {
        return true;
    }
    if($stmt === false) {
        die('prepare() failed:'. htmlspecialchars($stmt->error));
    }
}

?>

<!DOCTYPE html>
<div class="col-lg-12 mx-auto mb-5 text-white text-center">
            <h1 class="display-4">Voeg stagiair toe</h1>
            <p class="lead mb-0"> Hier kunt u stagiaires toevoegen. </p>
        </div>
        
            
                <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <?php foreach($errors as $error): ?>
                        <li>
                            <?php echo $error; ?>
                        </li>
                        <?php endforeach; ?>
                </div>
                <?php endif; ?>
                
                
                
                
                
                <div class="borderKL">
            <div class="bg-white rounded-lg p-5 shadow">
            <div>
                <a href="stagiaire.php" class="Terug"><span>Terug</span></a>
            </div>
                <h2 class="h6 font-weight-bold text-center mb-4">Stagiaires</h2>
                <?php
                $sql = "SELECT id, username, email, rol FROM users WHERE rol = 'User' AND supervisor_id IS NULL";
                if($stmt = $mysqli->prepare($sql)){
                    if(!$stmt->execute()){
                      echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                    }else{
                      $stmt->bind_result($id, $username, $email, $rol);
                    }
                    $stmt->close();
                  }else{
                    echo 'er zit een fout in de query:'.$mysqli->error;
                  }
                  $result = $conn->query($sql);
                   if ($result-> num_rows > 0) {
                       while ($row = $result-> fetch_assoc()) {

                ?>
                        <div>
                            <strong>Naam:</strong><?=$row['username'];?><br>
                            <strong>Email:</strong><?=$row['email'];?>
                            <form action="" method="post" class="form-group1">
                                <input type="hidden" name="id" value="<?=$row['id'];?>"/>
                                <input class="btn btn-primary btn-block btn-lg" type="submit" name="edit_student" value="Voeg stagiar toe">
                            </form>
                            
                            <?php
                           if(isset($_POST['submit'])){
                               $selected_val = $_POST['edit_st`udent'];  // Storing Selected Value In Variable
                               echo "De nieuwe rol is :" .$selected_val;  // Displaying Selected Value
                           }
                            ?>
                              
                            </a>
                            <hr class="HR">
                       </div>
                    <?php
                    }
                  }
                ?>

            </div>
        </div>        

