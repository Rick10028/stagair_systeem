<?php
require_once 'Controllers/AuthControllers.php';

if (isset($_GET['delete'])) {
    if($_SESSION['rol']== 'Leraar' or $_SESSION['rol']=='Admin'){
    $id = $_GET['delete'];
    $mysqli->query("DELETE FROM klassen WHERE id=$id") or die($mysqli->error());
    }
}

    require 'includes/header.php';
    require 'includes/navigation.php';
?>
    <!DOCTYPE html>
    <div class="col-lg-12 mx-auto mt-5 mb-5 text-white text-center">
            <h1 class="display-4">Klassenlijst</h1>
            <p class="lead mb-0"> Dit is een overzicht van alle klassen. </p>
        </div>  
        <div class="borderKL">
            <div class="bg-white rounded-lg p-5 shadow">
            <a class="Terug" href="add_klas.php">Voeg klas toe</a>
            <a href="add_UserToKlas.php" class="Terug"><span>Voeg leerling toe</span></a>
            <a href="add_teacher.php" class="Terug"><span>Voeg leraar toe</span></a>
                <h2 class="h6 font-weight-bold text-center mb-4">Klassen</h2>
                <?php
                   $sql = "SELECT id, name FROM klassen";

                   if($stmt = $mysqli->prepare($sql)){
                        if(!$stmt->execute()){
                            echo 'Uitvoeren van query mislukt' .$stmt->error.'in query'.$sql;
                        }   else {
                            $stmt->bind_result($id, $name);
                        }
                        $stmt->close();
                    } else{
                        echo 'er zit een fout in de query:'.$mysqli->error;
                    }

                   $result = $conn->query($sql);
                   if ($result-> num_rows > 0) {
                       while ($row = $result-> fetch_assoc()) {

                ?>
                        <div>
                            <?=$row['name'];?>
                            <?php
                              if(isset($_SESSION['id']) && $_SESSION['id'] == true){
                           ?>
                            <a href="klassen.php?delete=<?php echo $row['id']; ?>">
                                <div class="Klassen">Verwijderen</div>
                            </a>
                            <?php
                              }
                            ?>
                            <a href="view_klas.php?edit=<?php echo $row['id']; ?>">
                                <div class="Klassen">Bekijken</div>
                            </a>
                            <hr class="klassenHR">
                       </div>
                    <?php
                    }
                  }
                ?>
                <?php

                require_once 'includes/footer.php';

                ?>
