<?php

include "ConnectionDB/db.php"; // Using database connection file here


//getting id of the data from url
$id = $_GET['id'];
 
//deleting the row from table
$sql= $conn->prepare ("DELETE FROM users WHERE id=?");
$sql->bind_param("s", $id);
$sql->execute();

 
//redirecting to the display page (index.php in our case)
header("Location:admin.php");


?>
