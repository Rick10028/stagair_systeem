<?php

include('Controllers/AuthControllers.php');

if ($_SESSION['username'] == NULL) {
    header('location: login.php');
}

require_once 'includes/header.php';
require_once 'includes/navigation.php';

?>
<?php if ($_SESSION['rol'] == "User") {?>
<div class="container py-5">
    <div class="row">
        <div class="col-lg-12 mx-auto mb-5 text-white text-center">
            <h1 class="display-4">Uw Aanwezigheid</h1>
        </div>

        <div class="col-xl-3 col-lg-6 mb-4">
            <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4">Aanwezig</h2>
                <div class="row text-center mt-4">
                    <div class="col-12">
                        <div class="h4 font-weight-bold mb-0">24 uur</div><span class="small text-gray">Vorige week</span>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="h4 font-weight-bold mb-0">112 uur</div><span class="small text-gray">Vorige maand</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-lg-6 mb-4">
            <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4">Geoorloofd afwezig</h2>
                <div class="row text-center mt-4">
                    <div class="col-12">
                        <div class="h4 font-weight-bold mb-0">4 uur</div><span class="small text-gray">Vorige week</span>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="h4 font-weight-bold mb-0">20 uur</div><span class="small text-gray">Vorige maand</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-lg-6 mb-4">
            <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4">Ongeorloofd afwezig</h2>
                <div class="row text-center mt-4">
                    <div class="col-12">
                        <div class="h4 font-weight-bold mb-0">1 uur</div><span class="small text-gray">Vorige week</span>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="h4 font-weight-bold mb-0">9 uur</div><span class="small text-gray">Vorige maand</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-lg-6 mb-4">
            <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4">Niet geregistreed</h2>
                <div class="row text-center mt-4">
                    <div class="col-12">
                        <div class="h4 font-weight-bold mb-0">3 uur</div><span class="small text-gray">Vorige week</span>
                    </div>
                    <div class="col-12 mt-4">
                        <div class="h4 font-weight-bold mb-0">14,5 uur</div><span class="small text-gray">Vorige maand</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-12 col-lg-12 mt-5">
            <div class="bg-white rounded-lg p-5 shadow">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Dag</th>
                        <th scope="col">Inkloktijd</th>
                        <th scope="col">Uitkloktijd</th>
                        <th scope="col">Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">maandag</th>
                        <td>8:00</td>
                        <td>16:30</td>
                        <td>aanwezig</td>
                    </tr>
                    <tr>
                        <th scope="row">Dinsdag</th>
                        <td></td>
                        <td></td>
                        <td>afwezig</td>
                    </tr>
                    <tr>
                        <th scope="row">Woensdag</th>
                        <td>8:30</td>
                        <td>17:00</td>
                        <td>aanwezig</td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<?php }?>

<?php if ($_SESSION['rol'] == "Leraar" or $_SESSION['rol'] == "Admin") {?>
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-12 mx-auto mb-5 text-white text-center">
                <h1 class="display-4">Aanwezigheid Leerlingen</h1>
            </div>


            <div class="col-xl-12 col-lg-12 mt-5">
                <div class="bg-white rounded-lg p-5 shadow">
                    <div class="form-group pull-right">
                        <input type="text" class="search form-control" placeholder="Naam of klas student">
                    </div>
                    <span class="counter pull-right"></span>
                    <table class="table table-hover table-bordered results">
                        <thead>
                        <tr>
                            <th class="col-md-5 col-xs-5">Naam</th>
                            <th class="col-md-4 col-xs-4">Klas</th>
                            <th class="col-md-3 col-xs-3">Aanwezigheid</th>
                            <th class="col-md-3 col-xs-3">Actie</th>
                        </tr>
                        <tr class="warning no-result">
                            <td colspan="4"><i class="fa fa-warning"></i> Geen resultaten</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sql = "SELECT id, username, present, klas_id FROM users WHERE rol = 'User'";
                        $sql = "SELECT users.id, users.username, users.present, users.klas_id, klassen.name
                                FROM users
                                LEFT JOIN klassen 
                                ON users.klas_id = klassen.id";
                        if($stmt = $mysqli->prepare($sql)){
                            if(!$stmt->execute()){
                                echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                            }else{
                                $stmt->bind_result($id, $username, $present, $klas_id, $name);
                            }
                            $stmt->close();
                        }else{
                            echo 'er zit een fout in de query:'.$mysqli->error;
                        }
                        $result = $conn->query($sql);
                        if ($result-> num_rows > 0) {
                            while ($row = $result-> fetch_assoc()) {

                                ?>
                                <tr>
                                    <td class="text-primary font-weight-bold link"><a href="/show_profile.php?id=<?=$row['id']?>"> <?=$row['username'];?></a></td>
                                <td><?php if($row['name']==NULL){ ?><a class= "text-danger" href="/add_UserInKlas.php?edit=<?=$row['id'];?>">Voeg Leerling toe aan klas</a><?php
                                } else {echo $row['name'];}?>
                                </td><td><?=$row['present'];?></td>

                                    <td class="font-weight-bold"><a class="text-success" href="/chat.php">Stuur Bericht</td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
<?php }?>
<?php if ($_SESSION['rol'] == "Stage") {?>
    <div class="container py-5">
        <div class="row">
            <div class="col-lg-12 mx-auto mb-5 text-white text-center">
                <h1 class="display-4">Aanwezigheid Stagaires</h1>
            </div>


            <div class="col-xl-12 col-lg-12 mt-5">
                <div class="bg-white rounded-lg p-5 shadow">
                    <div class="form-group pull-right">
                        <input type="text" class="search form-control" placeholder="Naam of klas student">
                    </div>
                    <span class="counter pull-right"></span>
                    <table class="table table-hover table-bordered results">
                        <thead>
                        <tr>
                            <th class="col-md-5 col-xs-5">Naam</th>
                            <th class="col-md-4 col-xs-4">Bedrijf</th>
                            <th class="col-md-3 col-xs-3">Aanwezigheid</th>
                            <th class="col-md-3 col-xs-3">Actie</th>
                        </tr>
                        <tr class="warning no-result">
                            <td colspan="4"><i class="fa fa-warning"></i> Geen resultaten</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $id = $_SESSION['id'];
                        $sql = "SELECT id, username, present FROM users WHERE supervisor_id='$id'";
                        if($stmt = $mysqli->prepare($sql)){
                            if(!$stmt->execute()){
                                echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                            }else{
                                $stmt->bind_result($id, $username, $klas_id);
                            }
                            $stmt->close();
                        }else{
                            echo 'er zit een fout in de query:'.$mysqli->error;
                        }
                        $result = $conn->query($sql);
                        if ($result-> num_rows > 0) {
                            while ($row = $result-> fetch_assoc()) {

                                ?>
                                <tr>
                                    <td class="text-primary font-weight-bold link"><a href="/show_profile.php?id=<?=$row['id']?>"> <?=$row['username'];?></a></td>
                                    <td>dit is nog een test</td>
                                    <td><?=$row['present'];?></td>
                                    <td class="font-weight-bold"><a class="text-success" href="/chat.php">Stuur Bericht</td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
<?php }?>
<?php

require_once 'includes/footer.php';

?>
