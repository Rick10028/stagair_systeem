<?php
require_once 'Controllers/AuthControllers.php';

    require 'includes/header.php';
    require 'includes/navigation.php';

?>
    <!DOCTYPE html>
    <div class="col-lg-12 mx-auto mt-5 mb-5 text-white text-center">
        <h1 class="display-4">Leerling toevoegen</h1>
        <p class="lead mb-0"> Hier kunt u een leerling toevoegen een klas. </p>
    </div>  
        <div class="borderKL">
            <div class="bg-white rounded-lg p-5 shadow">
            <div>
                <a href="klassen.php" class="Terug"><span>Terug</span></a>
            </div>
                <h2 class="h6 font-weight-bold text-center mb-4">Leerlingen</h2>
                <?php
                $sql = "SELECT id, username, email FROM users WHERE rol = 'User' AND klas_id ='0'";
                if($stmt = $mysqli->prepare($sql)){
                    if(!$stmt->execute()){
                      echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                    }else{
                      $stmt->bind_result($id, $username, $email);
                    }
                    $stmt->close();
                  }else{
                    echo 'er zit een fout in de query:'.$mysqli->error;
                  }
                  $result = $conn->query($sql);
                   if ($result-> num_rows > 0) {
                       while ($row = $result-> fetch_assoc()) {

                ?>
                        <div>
                            <strong>Naam:</strong><?=$row['username'];?><br>
                            <strong>Email:</strong><?=$row['email'];?>
                            
                            <a href="add_UserInKlas.php?edit=<?php echo $row['id']; ?>">
                                <div class="Klassen">Toevoegen</div>
                            </a>
                            <hr class="klassenHR">
                       </div>
                    <?php
                    }
                  }
                ?>

            </div>
        </div>