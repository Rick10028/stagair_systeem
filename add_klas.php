<?php
require_once 'Controllers/AuthControllers.php';

if($_SESSION['username']==NULL){
    header('location: login.php');
}

if (isset($_POST['klassen'])) {
  $name = $_POST['name'];
  signupAdminPnl($name);
}

function signupAdminPnl($name) {
  global $errors;
  //Give errors when values are incorrect.
    if (empty($name)) {
        $errors['name'] = "Naam verplicht invullen";
    }

  if (count($errors) === 0) $user_id = insertUserAdminPnl($name);
  if (count($errors) === 0) redirectToPages();
}

function insertUserAdminPnl($name) {
  global $conn;
  $verified = false;
    $sql = "INSERT INTO klassen (name) VALUES(?)";
  $stmt = $conn->prepare($sql);
  if(false === $stmt) {
    die('prepare() failed:' . htmlspecialchars($stmt->error));
  }
  $stmt->bind_param('s', $name);
  if ( false === $stmt ) {
    die('bind_param() failed:' . htmlspecialchars($stmt->error));
  }
  if ($stmt->execute()) {
    $user_id = $conn->insert_id;
    return $user_id;

    exit();
  } else {
    die("Oeps! Er is iets mis gegaan tijdens het aanmaken van jouw account!" . $stmt->error);
  }

}

function redirectToPages() {
  header('location: klassen.php');
  exit();
}

require 'includes/header.php';
require 'includes/navigation.php';

?>

<!DOCTYPE html>
<div class="col-lg-12 mx-auto mb-5 text-white text-center">
            <h1 class="display-4">Voeg klas toe</h1>
            <p class="lead mb-0"> Hier kunt u klassen aanmaken. </p>
        </div>
        <div class="borderKL">
            <div class="bg-white rounded-lg p-5 shadow">
            
                <h2 class="h6 font-weight-bold text-center mb-4">Klassen</h2>
                <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <?php foreach($errors as $error): ?>
                        <li>
                            <?php echo $error; ?>
                        </li>
                        <?php endforeach; ?>
                </div>
                <?php endif; ?>
                    <form action="" method="post">
                        <div class="form-group">
                            <label for="title">klas naam:</label>
                            <input type="text" name="name" value="" class="form-control form conrtol-lg">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="klassen" class="btn btn-primary btn-block btn-lg">Voeg pagina toe</button>
                        </div>
                    </form>   
            </div>
        </div>