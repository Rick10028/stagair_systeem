<?php
require_once 'Controllers/AuthControllers.php';

if (isset($_GET['delete'])) {
    if($_SESSION['rol']== 'Stage'||'Admin'){
        $id = $_GET['delete'];
        $mysqli->query("UPDATE users SET supervisor_id=NULL WHERE id='$id'") or die($mysqli->error());
    }
}

require 'includes/header.php';
require 'includes/navigation.php';
?>
<!DOCTYPE html>
<div class="col-lg-12 mx-auto mt-5 mb-5 text-white text-center">
    <h1 class="display-4">Uw Stagiaires</h1>
    <p class="lead mb-0"> Dit is een overzicht van al uw stagiaires. </p>
</div>
<div class="borderKL">
    <div class="bg-white rounded-lg p-5 shadow">
        <a href="add_stagiare.php" class="Terug"><span>Voeg Stagiair toe</span></a>
        <h2 class="h6 font-weight-bold text-center mb-4">Uw stagiares</h2>
        <?php
        $id = $_SESSION['id'];
        $sql = "SELECT id, username FROM users WHERE supervisor_id='$id'";

        if($stmt = $mysqli->prepare($sql)){
            if(!$stmt->execute()){
                echo 'Uitvoeren van query mislukt' .$stmt->error.'in query'.$sql;
            }   else {
                $stmt->bind_result($id, $username);
            }
            $stmt->close();
        } else{
            echo 'er zit een fout in de query:'.$mysqli->error;
        }

        $result = $conn->query($sql);
        if ($result-> num_rows > 0) {
            while ($row = $result-> fetch_assoc()) {

                ?>
                <div>
                    <?=$row['username'];?>
                    <?php
                    if(isset($_SESSION['id']) && $_SESSION['id'] == true){
                        ?>
                        <a href="stagiaire.php?delete=<?php echo $row['id']; ?>">
                            <div class="Klassen">Verwijderen</div>
                        </a>
                        <?php
                    }
                    ?>
                    <hr class="HR">
                </div>
                <?php
            }
        }
        ?>
        <?php

        require_once 'includes/footer.php';

        ?>
