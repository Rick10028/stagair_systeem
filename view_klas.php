<?php
require_once 'Controllers/AuthControllers.php';

require 'includes/header.php';
require 'includes/navigation.php';


$refresh = '0';
if (isset($_GET['delete'])) {
    if($_SESSION['rol']== 'Leraar'||'Admin'){
        $id = $_GET['delete'];
        $mysqli->query("UPDATE users SET klas_id = '0' WHERE id='$id'") or die($mysqli->error());
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
        exit;
    }
}

?>

<!DOCTYPE html>
<div class="col-lg-12 mx-auto mb-5 text-white text-center">
    <h1 class="display-4">Klas</h1>
    <p class="lead mb-0"> Dit is een overzicht van alle Leerlingen. </p>
</div>
<div class="borderKL">
    <div class="bg-white rounded-lg p-5 shadow">
        <div>
            <a href="klassen.php" class="Terug"><span>Terug</span></a>
        </div>
            <?php
                $id = $_GET['edit'];
                $sql = "SELECT id, username, email, rol FROM users WHERE klas_id = (SELECT id FROM klassen WHERE id='$id') AND rol= 'Leraar'";
                if($stmt = $mysqli->prepare($sql)){
                    if(!$stmt->execute()){
                        echo 'Uitvoeren van query mislukt' .$stmt->error.'in query'.$sql;
                    }   else {
                        $stmt->bind_result($id, $name, $email, $rol);
                    }
                    $stmt->close();
                } else{
                    echo 'er zit een fout in de query:'.$mysqli->error;
                }

                $result = $conn->query($sql);
                if ($result-> num_rows > 0) ?> <h2 class="h6 font-weight-bold text-center mb-4">docenten</h2> <?php {
                    while ($row = $result-> fetch_assoc()) {

            ?>
                <div>
                    <strong>Naam:</strong><?=$row['username'];?><br>
                    <strong>Email:</strong><?=$row['email'];?>
                    <a href="view_klas.php?delete=<?php echo $row['id']; ?>">
                        <div class="Klassen">Verwijderen</div>
                    </a>
                    <hr class="klassenHR">
                </div>
                <?php
                    }
                  }
                ?>

        <h2 class="h6 font-weight-bold text-center mb-4">Leerlingen</h2>
            <?php
                $id = $_GET['edit'];
                $sql = "SELECT id, username, email FROM users WHERE klas_id = (SELECT id FROM klassen WHERE id='$id') AND rol= 'User'";
                if($stmt = $mysqli->prepare($sql)){
                    if(!$stmt->execute()){
                        echo 'Uitvoeren van query mislukt' .$stmt->error.'in query'.$sql;
                    }   else {
                        $stmt->bind_result($id, $name, $email);
                    }
                    $stmt->close();
                } else{
                    echo 'er zit een fout in de query:'.$mysqli->error;
                }

                $result = $conn->query($sql);
                if ($result-> num_rows > 0) {
                    while ($row = $result-> fetch_assoc()) {

            ?>
                <div>
                    <strong>Naam:</strong><?=$row['username'];?><br>
                    <strong>Email:</strong><?=$row['email'];?>
                    <a href="view_klas.php?delete=<?php echo $row['id']; ?>">
                        <div class="Klassen">Verwijderen</div>
                    </a>
                    <hr class="klassenHR">
                </div>
                <?php
                    }
                  }
                ?>
    </div>
</div>