<!DOCTYPE HTML>
<html>
<?php
    include('Controllers/AuthControllers.php');

    if($_SESSION['username']==NULL){
        header('location: login.php');
    }

    require_once 'includes/header.php';
    require_once 'includes/navigation.php';
    $link_address = '#';
  
?>
<div class="container-fluid py-5">
    <div class="row">

        <!-- For demo purpose -->
        <div class="col-lg-12 mx-auto mb-5 text-white text-center">
            <h1 class="display-4">Welkom <?php echo $_SESSION['username']; ?>
            </h1><p class="lead mb-0">
            <?php if ($_SESSION['rol'] == "User") {
                 echo "Via dit systeem kunt u uw aanwezigheid bijhouden.";
            }
            if ($_SESSION['rol'] == "Leraar") {
                echo "Via dit systeem kunt u uw contact opnemen met Stage Begeleiders en Studenten en hun aanwezigheid inzien.";
            }
            if ($_SESSION['rol'] == "Stage") {
                echo "Zie uw stagaires aanwezigheid in en neem mogelijk contact op met de leraar.";
            }
              if ($_SESSION['rol'] == "Admin") {
                  echo "U hebt toegang tot alle data en aanpassingen.";
            }
            ?>
            <p class="lead">in
                    <u>constructie..</u></a>
            </p>
        </div>
        <!-- END -->
        <?php if ($_SESSION['rol'] == "User") {?>
            <div class="container py-5">
                <div class="row">
                <div class="col-xl-3 col-lg-6 mb-4">
                    <div class="bg-white rounded-lg p-5 shadow">
                        <h2 class="h6 font-weight-bold text-center mb-4">Geoorloofd</h2>

                        <!-- Progress bar 1 -->
                        <div class="progress mx-auto" data-value='91'>
                  <span class="progress-left">
                                <span class="progress-bar border-primary"></span>
                  </span>
                            <span class="progress-right">
                                <span class="progress-bar border-primary"></span>
                  </span>
                            <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">91<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->

                        <!-- Demo info -->
                        <div class="row text-center mt-4">
                            <div class="col-6 border-right">
                                <div class="h4 font-weight-bold mb-0">28%</div><span class="small text-gray">Vorige week</span>
                            </div>
                            <div class="col-6">
                                <div class="h4 font-weight-bold mb-0">60%</div><span class="small text-gray">Vorige maand</span>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 mb-4">
                    <div class="bg-white rounded-lg p-5 shadow">
                        <h2 class="h6 font-weight-bold text-center mb-4">aanwezig</h2>

                        <!-- Progress bar 2 -->
                        <div class="progress mx-auto" data-value='85'>
                  <span class="progress-left">
                                <span class="progress-bar border-success"></span>
                  </span>
                            <span class="progress-right">
                                <span class="progress-bar border-success"></span>
                  </span>
                            <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">85<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->

                        <!-- Demo info-->
                        <div class="row text-center mt-4">
                            <div class="col-6 border-right">
                                <div class="h4 font-weight-bold mb-0">28%</div><span class="small text-gray">Vorige week</span>
                            </div>
                            <div class="col-6">
                                <div class="h4 font-weight-bold mb-0">60%</div><span class="small text-gray">Vorige maand</span>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 mb-4">
                    <div class="bg-white rounded-lg p-5 shadow">
                        <h2 class="h6 font-weight-bold text-center mb-4">afwezig</h2>

                        <!-- Progress bar 3 -->
                        <div class="progress mx-auto" data-value='15'>
                  <span class="progress-left">
                                <span class="progress-bar border-warning"></span>
                  </span>
                            <span class="progress-right">
                                <span class="progress-bar border-warning"></span>
                  </span>
                            <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">15<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->

                        <!-- Demo info -->
                        <div class="row text-center mt-4">
                            <div class="col-6 border-right">
                                <div class="h4 font-weight-bold mb-0">28%</div><span class="small text-gray">Vorige week</span>
                            </div>
                            <div class="col-6">
                                <div class="h4 font-weight-bold mb-0">60%</div><span class="small text-gray">Vorige maand</span>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 mb-4">
                    <div class="bg-white rounded-lg p-5 shadow">
                        <h2 class="h6 font-weight-bold text-center mb-4">Ongeoorloofd</h2>

                        <!-- Progress bar 4 -->
                        <div class="progress mx-auto" data-value='9'>
                  <span class="progress-left">
                                <span class="progress-bar border-danger"></span>
                  </span>
                            <span class="progress-right">
                                <span class="progress-bar border-danger"></span>
                  </span>
                            <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                                <div class="h2 font-weight-bold">9<sup class="small">%</sup></div>
                            </div>
                        </div>
                        <!-- END -->

                        <!-- Demo info -->
                        <div class="row text-center mt-4">
                            <div class="col-6 border-right">
                                <div class="h4 font-weight-bold mb-0">28%</div><span class="small text-gray">Vorige week</span>
                            </div>
                            <div class="col-6">
                                <div class="h4 font-weight-bold mb-0">60%</div><span class="small text-gray">Vorige maand</span>
                            </div>
                        </div>
                        <!-- END -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>

<?php if ($_SESSION['rol'] == "Leraar") {?>
    <div class="col-xl-1 col-lg-6 mt-5"></div>
        <div class="col-xl-5 col-lg-6 mt-5">
            <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4">Belangrijk</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Naam</th>
                        <th scope="col">aanwezigheid</th>
                        <th scope="col">actie</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql = "SELECT id, username, present FROM users WHERE rol = 'User'";
                        if($stmt = $mysqli->prepare($sql)){
                            if(!$stmt->execute()){
                                echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                            }else{
                                $stmt->bind_result($id, $username, $klas_id);
                            }
                            $stmt->close();
                        }else{
                            echo 'er zit een fout in de query:'.$mysqli->error;
                        }
                        $result = $conn->query($sql);
                        if ($result-> num_rows > 0) {
                            while ($row = $result-> fetch_assoc()) {

                                ?>
                                <tr>
                                    <td><?=$row['username'];?></td>
                                    <td><?=$row['present'];?></td>
                                    <td class="font-weight-bold"><a class="text-success" href="/chat.php">Stuur Bericht</td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                </table>

            </div>
        </div>

    <div class="col-xl-5 col-lg-6 mt-5">
        <div class="bg-white rounded-lg p-5 shadow">
            <h2 class="h6 font-weight-bold text-center mb-4">Nieuwe Berichten</h2>
            <table class="table">
            <tbody>
                        <?php
                        $sql = "SELECT id, username, present FROM users WHERE rol = 'User'";
                        if($stmt = $mysqli->prepare($sql)){
                            if(!$stmt->execute()){
                                echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                            }else{
                                $stmt->bind_result($id, $username, $klas_id);
                            }
                            $stmt->close();
                        }else{
                            echo 'er zit een fout in de query:'.$mysqli->error;
                        }
                        $result = $conn->query($sql);
                        if ($result-> num_rows > 0) {
                            while ($row = $result-> fetch_assoc()) {

                                ?>
                                <tr>
                                    <td><?=$row['username'];?></td>
                                    <td>Hoelaat morgen les?</td>
                                    <td class="font-weight-bold"><a class="text-success" href="/chat.php">Stuur Bericht</td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
            </table>

        </div>
    </div>
    <div class="col-xl-1 col-lg-6 mt-5"></div>

    </div>
        <?php }?>

<?php if ($_SESSION['rol'] == "Stage") {?>
    <div class="col-xl-1 col-lg-6 mt-5"></div>
    <div class="col-xl-5 col-lg-6 mt-5">
        <div class="bg-white rounded-lg p-5 shadow">
            <h2 class="h6 font-weight-bold text-center mb-4">Uw Stagaires</h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Naam</th>
                    <th scope="col">aanwezigheid</th>
                    <th scope="col">actie</th>
                </tr>
                </thead>
                <tbody>
                        <?php
                        $sql = "SELECT id, username, present FROM users WHERE rol = 'User'";
                        if($stmt = $mysqli->prepare($sql)){
                            if(!$stmt->execute()){
                                echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                            }else{
                                $stmt->bind_result($id, $username, $klas_id);
                            }
                            $stmt->close();
                        }else{
                            echo 'er zit een fout in de query:'.$mysqli->error;
                        }
                        $result = $conn->query($sql);
                        if ($result-> num_rows > 0) {
                            while ($row = $result-> fetch_assoc()) {

                                ?>
                                <tr>
                                    <td><?=$row['username'];?></td>
                                    <td><?=$row['present'];?></td>
                                    <td class="font-weight-bold"><a class="text-success" href="/chat.php">Stuur Bericht</td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
            </table>

        </div>
    </div>

    <div class="col-xl-5 col-lg-6 mt-5">
        <div class="bg-white rounded-lg p-5 shadow">
            <h2 class="h6 font-weight-bold text-center mb-4">Nieuwe Berichten</h2>
            <table class="table">
            <tbody>
                        <?php
                        $sql = "SELECT id, username, present FROM users WHERE rol = 'User'";
                        if($stmt = $mysqli->prepare($sql)){
                            if(!$stmt->execute()){
                                echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                            }else{
                                $stmt->bind_result($id, $username, $klas_id);
                            }
                            $stmt->close();
                        }else{
                            echo 'er zit een fout in de query:'.$mysqli->error;
                        }
                        $result = $conn->query($sql);
                        if ($result-> num_rows > 0) {
                            while ($row = $result-> fetch_assoc()) {

                                ?>
                                <tr>
                                    <td><?=$row['username'];?></td>
                                    <td>Hoelaat morgen aanwezig?</td>
                                    <td class="font-weight-bold"><a class="text-success" href="/chat.php">Stuur Bericht</td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
            </table>

        </div>
    </div>
    <div class="col-xl-1 col-lg-6 mt-5"></div>

    </div>
<?php }?>
<?php

    require_once 'includes/footer.php';
    
?>

</html>
