<?php
require_once 'Controllers/AuthControllers.php';

if($_SESSION['username']==NULL){
    header('location: login.php');
}

if (isset($_POST['dateout'])) {
  $name = $_POST['name'];
  signupAdminPnl($name);
}

if (isset($_POST['datein'])) {
  $name = $_POST['name'];
  signupAdminPnl($name);
}

function signupAdminPnl($name) {
  global $errors;
  //Give errors when values are incorrect.
    if (empty($name)) {
        $errors['name'] = "Naam verplicht invullen";
    }

  if (count($errors) === 0) $user_id = insertUserAdminPnl($name);
  if (count($errors) === 0) redirectToPages();
}

function insertUserAdminPnl($name) {
  global $conn;
  $verified = false;
    $sql = "INSERT INTO date (name) VALUES(?)";
  $stmt = $conn->prepare($sql);
  if(false === $stmt) {
    die('prepare() failed:' . htmlspecialchars($stmt->error));
  }
  $stmt->bind_param('s', $name);
  if ( false === $stmt ) {
    die('bind_param() failed:' . htmlspecialchars($stmt->error));
  }
  if ($stmt->execute()) {
    $user_id = $conn->insert_id;
    return $user_id;

    exit();
  } else {
    die("Oeps! Er is iets mis gegaan tijdens het aanmaken van jouw account!" . $stmt->error);
  }

}

function redirectToPages() {
  header('location: index.php');
  exit();
}

require 'includes/header.php';
require 'includes/navigation.php';

$id = $_SESSION['id'];
$sql="SELECT id, username FROM users WHERE id='$id'";


if($stmt = $mysqli->prepare($sql)){
    if(!$stmt->execute()){
        echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
    }else{
        $stmt->bind_result($id, $username);
    }
    $stmt->close();
}else{
    echo 'er zit een fout in de query:'.$mysqli->error;
}
$result = $conn->query($sql);

if($result-> num_rows>0){
    while($row = $result-> fetch_assoc()){

?>

<!DOCTYPE html>
<div class="col-lg-12 mx-auto mb-5 text-white text-center">
            <h1 class="display-4">Aanmelden of afmelden</h1>
            <p class="lead mb-0"> Hier kunt je jezelf aanmelden of afmelden. </p>
        </div>
        <div class="borderKL">
            <div class="bg-white rounded-lg p-5 shadow">
            
                <h2 class="h6 font-weight-bold text-center mb-4">Vul hier je naam in:</h2>
                <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <?php foreach($errors as $error): ?>
                        <li>
                            <?php echo $error; ?>
                        </li>
                        <?php endforeach; ?>
                </div>
                <?php endif; ?>
                    <form action="" method="post">
                        
                        <input type="text" name="name" value="<?php echo $row['username'] ?>" class="form-control form conrtol-lg"><br >

                        <h2 class="h6 font-weight-bold text-center mb-4">Kies hier of je wilt aanmelden of afmelden:</h2>
                        <div class="form-group">
                            <button type="submit" name="datein" class="btn btn-primary btn-block btn-lg">Aanmelden</button>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="dateout" class="btn btn-primary btn-block btn-lg">Afmelden</button>
                        </div>
                    </form> 
                    <?php
        }
    }
    ?>  
            </div>
        </div>