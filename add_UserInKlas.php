<?php

include('Controllers/AuthControllers.php');

if($_SESSION['username']==NULL){
    header('location: login.php');
}

    require_once 'includes/header.php';
    require_once 'includes/navigation.php';

    if(isset($_POST['edit_student'])) {
        $id = $_POST['id'];
        $klas_id = $_POST['klas_id'];
    
        if (editUser($id, $klas_id)){
            echo "Gebruiker veranderd, Druk op Terug om de verandering te zien.";
        }
    } else {
        $id=$_GET['edit'];
    }
        // Returns true if succes, return false if user gives wrong input //
        // dies on error //
    function editUser($id, $klas_id) {

        global $conn;
        $sql = "UPDATE users SET klas_id=? WHERE id='$id'";
        $stmt = $conn->prepare($sql);
        if($stmt === false) {
            die('prepare failed:'.htmlspecialchars($stmt->error));
        }
        $stmt->bind_param('s', $klas_id);
        if($stmt->execute()) {
            return true;
        }
        if($stmt === false) {
            die('prepare() failed:'. htmlspecialchars($stmt->error));
        }
    }

?>
<!DOCTYPE html>

<div class="col-lg-12 mx-auto mt-5 mb-5 text-white text-center">
        <h1 class="display-4">Leerling toevoegen</h1>
        <p class="lead mb-0"> Hier kunt u een leerling toevoegen een klas. </p>
    </div>
        <div class="borderKL">
            <div class="bg-white rounded-lg p-5 shadow">
            <div>
                <a href="add_UserToKlas.php" class="Terug"><span>Terug</span></a>
            </div>
                <h2 class="h6 font-weight-bold text-center mb-4">Selecteer klas</h2>
                <form action="" method="post" class="form-group1">
            <input type="hidden" name="id" value="<?php echo $id; ?>">
                <?php
                $sql = "SELECT * FROM klassen";
                if($stmt = $mysqli->prepare($sql)){
                  if(!$stmt->execute()){
                    echo 'uitvoeren van query mislukt'.$stmt->close().'in query'.$sql;
                  }else{
                    $stmt->bind_result($id,$name);
                  }
                  $stmt->close();
                }else{
                  echo 'er zit een fout in de query:'.$mysqli->error;
                }
                $result = $conn->query($sql);
                if ($result-> num_rows > 0)?> <select  name="klas_id"> <?php  {
                    while ($row = $result-> fetch_assoc()) {

                        ?>
                        <option value="<?=$row['id']?>"><?=$row['name']?></option>
                        <?php
                    }
                }
                ?>
                    </select>
            <br>
            <input class="btn btn-primary btn-block btn-lg" type="submit" name="edit_student" value="Voeg klas toe">
        </form>
            </div>
        </div>
    </div>
</body>
</html>
<?php
        require_once 'includes/footer.php';
    ?>