<!DOCTYPE HTML>
<?php
include('Controllers/AuthControllers.php');

require_once 'includes/header.php';
require_once 'includes/navigation.php';


?>
<div class="container py-5">
    <div class="col-lg-12 mx-auto mb-5 text-white text-center">
        <h1 class="display-4">Alle gebruikers</h1>
    </div>
    <div class="bg-white rounded-lg p-3 shadow">
        <div class="container-xl">
            <div>
                <a href="admin.php" class="Terug"><span>Terug</span></a>
            </div>
            <div class="table-responsive">
                <div class="table-wrapper">
                    <body>
                    <div class="borderlogin">
                        <?php if(count($errors) > 0): ?>
                            <div class="alert alert-danger">
                                <?php foreach($errors as $error): ?>
                                    <li><?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-12" form-div>
                            <form action="" method="post">

                                <div class="form-group">
                                    <label for="username">Naam:</label>
                                    <input type="text" name="username"  value="" class="form-control form conrtol-lg">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="text" name="email" value="" class="form-control form conrtol-lg">
                                </div>
                                <div class="form-group">
                                    <label for="password">Wachtwoord:</label>
                                    <input type="password" name="password" class="form-control form conrtol-lg">
                                </div>
                                <div class="form-group">
                                    <label for="passwordConf">Herhaal wachtwoord:</label>
                                    <input type="password" name="passwordConf"  class="form-control form conrtol-lg">
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="signup-btn" class="btn btn-primary btn-block btn-lg">Add user</button>
                                </div>

                            </form>
                        </div>
                    </div>
                    </body>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php

require_once 'includes/footer.php';

?>

    