<!DOCTYPE HTML>
<html>
<?php
include('Controllers/AuthControllers.php');

require_once 'includes/header.php';
require_once 'includes/navigation.php';

    $id=$_GET['id'];

// Returns true if succes, return false if user gives wrong input //
// dies on error //


$sql="SELECT id, username, email, company, supervisor_id, klas_id FROM users WHERE id='$id'";

if($stmt = $mysqli->prepare($sql)){
    if(!$stmt->execute()){
        echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
    }else{
        $stmt->bind_result($id, $username, $email, $company, $supervisorId, $klasId);
    }
    $stmt->close();
}else{
    echo 'er zit een fout in de query:'.$mysqli->error;
}
$result = $conn->query($sql);

if($result-> num_rows>0){
while($row = $result-> fetch_assoc()){
$supervisorId = $row['supervisor_id'];
$klasId = $row['klas_id'];

?>
    <div class="col-lg-12 mx-auto mt-5 text-white text-center">
        <h1 class="display-4"><?php echo $row['username']; ?>'s profile </h1>
        </p>
    </div>

<div class="container mt-5">
    <div class="bg-white rounded-lg p-5 shadow">
        <a onclick="history.back()" class="Terug"><span>Terug</span></a>

        <div class="row">
            <div class="col-lg-4 pb-5">
                <!-- Account Sidebar-->
                <div class="author-card pb-3">
                    <div class="author-card-cover" style="background-image: url(https://demo.createx.studio/createx-html/img/widgets/author/cover.jpg);"><a class="btn btn-style-1 btn-white btn-sm" href="#" data-toggle="tooltip" title=""><i class="fa fa-award text-md"></i>&nbsp;Aanwezig</a></div>
                    <div class="author-card-profile">
                        <div class="author-card-avatar"><img src="images/profiel.jpg" alt="<?php echo $row['username'] ?>"">
                        </div>
                        <div class="author-card-details">
                            <h5 class="author-card-name text-lg"><?php echo $row['username'] ?></h5><span class="author-card-position">Joined February 06, 2017</span>
                        </div>
                    </div>
                </div>
                <div class="wizard">

                    <!--This is a loop for all supervisors, shows all interns-->
                    <?php
                    }
                    }
                    $sql="SELECT id, username FROM users WHERE supervisor_id = '$id'";

                    if($stmt = $mysqli->prepare($sql)){
                        if(!$stmt->execute()){
                            echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
                        }else{
                            $stmt->bind_result( $id, $username);
                        }
                        $stmt->close();
                    }else{
                        echo 'er zit een fout in de query:'.$mysqli->error;
                    }
                    $result = $conn->query($sql);

                    if($result-> num_rows>0){?>
                    <nav class="list-group list-group-flush">
                        <a class="list-group-item active" href="#"><i class="fe-icon-user text-muted"></i>Stagiares</a>
                    </nav><?php
                        while($row = $result-> fetch_assoc()){

                            ?>
                            <a class="list-group-item" href="/show_profile.php?id=<?=$row['id']?>">
                                <div class="d-flex justify-content-between align-items-center">
<div><i class="fe-icon-heart mr-1 text-muted"></i>
                                        <div class="d-inline-block font-weight-medium text-uppercase"><?php echo $row['username'] ?></div>
                                    </div>
                                </div>
                            </a>

                            <!--This is a loop for all interns, shows their supervisor-->
                            <?php
                        }
                    }
                    $sql="SELECT id, username FROM users WHERE '$supervisorId' = id";

                    if($stmt = $mysqli->prepare($sql)){
                        if(!$stmt->execute()){
                            echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
                        }else{
                            $stmt->bind_result( $id, $username);
                        }
                        $stmt->close();
                    }else{
                        echo 'er zit een fout in de query:'.$mysqli->error;
                    }
                    $result = $conn->query($sql);

                    if($result-> num_rows>0){?>
                    <nav class="list-group list-group-flush">
                        <a class="list-group-item active" href="#"><i class="fe-icon-user text-muted"></i>Stage begeleider</a>
                    </nav><?php
                        while($row = $result-> fetch_assoc()){

                            ?>
                            <a class="list-group-item" href="/show_profile.php?id=<?=$row['id']?>">
                                <div class="d-flex justify-content-between align-items-center">
<div><i class="fe-icon-heart mr-1 text-muted"></i>
                                        <div class="d-inline-block font-weight-medium text-uppercase"><?php echo $row['username'] ?></div>
                                    </div>
                                </div>
                            </a>
                            <!--This is a loop for all students to show their teacher-->
                            <?php
                        }
                    }
                    $sql="SELECT id, username FROM users WHERE $klasId = klas_id AND rol = 'Leraar'";

                    if($stmt = $mysqli->prepare($sql)){
                        if(!$stmt->execute()){
                            echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
                        }else{
                            $stmt->bind_result( $id, $username);
                        }
                        $stmt->close();
                    }else{
                        echo 'er zit een fout in de query:'.$mysqli->error;
                    }
                    $result = $conn->query($sql);

                    if($result-> num_rows>0){?>
                        <nav class="list-group list-group-flush">
                            <a class="list-group-item active" href="#"><i class="fe-icon-user text-muted"></i>Docent</a>
                        </nav><?php
                        while($row = $result-> fetch_assoc()){

                            ?>
                            <a class="list-group-item"  href="/show_profile.php?id=<?=$row['id']?>">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div><i class="fe-icon-heart mr-1 text-muted"></i>
                                        <div class="d-inline-block font-weight-medium text-uppercase"><?php echo $row['username'] ?></div>
                                    </div>
                                </div>
                            </a>
                            <?php
                        }
                    }
                    $sql="SELECT id, username, email, company FROM users WHERE id='$id'";

                    if($stmt = $mysqli->prepare($sql)){
                        if(!$stmt->execute()){
                            echo 'uitvoeren van query mislukt' .$stmt->error.'in query'.sql;
                        }else{
                            $stmt->bind_result($id, $username, $email, $company);
                        }
                        $stmt->close();
                    }else{
                        echo 'er zit een fout in de query:'.$mysqli->error;
                    }
                    $result = $conn->query($sql);

                    if($result-> num_rows>0){
                    while($row = $result-> fetch_assoc()){
                    ?>
                </div>
            </div>
            <!-- Profile Settings-->
            <div class="col-lg-8 pb-5">
                <form class="row" action="" method="post">
                    <input type="hidden" name="id" value="<?php echo $id; ?>">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 13px; color: grey" for="account-fn">username</label>
                            <p style="font-size: 20px"><?php echo $row['username'] ?></p>
                        </div>
                    </div>
                    <?php if ($row['company'] == NULL) {} else{?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 13px; color: grey"for="account-company">Bedrijf</label>
                            <p style="font-size: 20px"><?php echo $row['company'] ?></p>

                        </div>
                    </div>
                    <?php } ?>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size: 13px; color: grey" for="account-email">E-mail Address</label>
                            <p style="font-size: 20px"><?php echo $row['email'] ?></p>
                        </div>
                    </div>


                    <div class="col-12">
                        <hr class="mt-2 mb-3">
                        <div class="d-flex flex-wrap justify-content-between align-items-center">
                            <div class="custom-control custom-checkbox d-block">
                                <input class="custom-control-input" type="checkbox" id="subscribe_me" checked="">
                            </div>
                        </div>
                    </div>
                </form>

                <a class="text-success font-weight-bold" href="/chat.php">Stuur Bericht</a>
            </div>
        </div>
    </div>
</div>
<?php
}
}
require_once 'includes/footer.php';

?>
</div>
</html>
