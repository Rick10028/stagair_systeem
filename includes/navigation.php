<?php
require_once 'controllers/authControllers.php';
?>

<!DOCTYPE html>
<html>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Stagair systeem</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
          <?php if ($_SESSION['rol'] == "Leraar") {?>
          <a class="nav-link" href="presence.php">Leerlingen</a>
          <?php }
          elseif ($_SESSION['rol'] == "Stage") {?>
          <a class="nav-link" href="presence.php">Stagiaires</a>
          <?php }
          else{?>
          <a class="nav-link" href="presence.php">Aanwezigheid</a>
          <?php } ?>

      </li>
        <?php if ($_SESSION['rol'] == "Leraar" or $_SESSION['rol'] == "Admin") {?>
      <li class="nav-item">
        <a class="nav-link" href="klassen.php">Klassen</a>
      </li>
        <?php } ?>
        <?php if ($_SESSION['rol'] == "Stage" or $_SESSION['rol'] == "Admin") {?>
          <li class="nav-item">
            <a class="nav-link" href="stagiaire.php">Stagiaires</a>
          </li>
        <?php } ?>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="chat.php">Chat</a>
      </li>
        <?php if ($_SESSION['rol'] == "Admin") {?>
            <li class="nav-item">
                <a class="nav-link" href="admin.php">Gebruikers</a>
            </li>
        <?php } ?>
      
       <li class="nav-item">
        <a class="nav-link" href="aanmeldenafmelden.php">Aanmelden/afmelden</a>
      </li>
    </ul>
    <div class="dropdown">
      <a class="btn" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-user"></i> <?php echo $_SESSION['username']; ?>'s Profiel <i class="fas fa-sort-down"></i>
      </a>
      <div class="dropdown-menu " aria-labelledby="dropdownMenu">
        <a class="dropdown-item" href="profile.php?edit=<?php echo $_SESSION['id']; ?>">mijn profiel</a>
        <a href="login.php?logout=1" class="dropdown-item">Uitloggen</a>
      </div>
    </div>
  </div>
</nav>


