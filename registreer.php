<?php
require_once 'Controllers/AuthControllers.php';
?>

<!DOCTYPE html>
<html lang="nl">
<head>
  <meta charset="utf-8">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Registreer</title>
</head>
<body>
  <center><h1>Registreren</h1></center>
            <div class="borderlogin">
              <?php if(count($errors) > 0): ?>
                      <div class="alert alert-danger">
                          <?php foreach($errors as $error): ?>
                              <li><?php echo $error; ?></li>
                          <?php endforeach; ?>
                      </div>
                  <?php endif; ?>
                <div class="col-md-4 offset-md-4" form-div>
                    <form action="" method="post">
                        <h3 class="text-center">Registreer</h3>
                    <div class="form-group">
                        <label for="username">Naam:</label>
                        <input type="text" name="username"  value="" class="form-control form conrtol-lg">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="text" name="email" value="" class="form-control form conrtol-lg">
                    </div>
                    <div class="form-group">
                        <label for="password">Wachtwoord:</label>
                        <input type="password" name="password" class="form-control form conrtol-lg">
                    </div>
                    <div class="form-group">
                        <label for="passwordConf">Herhaal wachtwoord:</label>
                        <input type="password" name="passwordConf"  class="form-control form conrtol-lg">
                    </div>
                    <div class="form-group">
                        <button type="submit" name="signup-btn" class="btn btn-primary btn-block btn-lg">Registreer</button>
                    </div>
                    <p class="text-center">Bent u al een gebruiker?<a href="login.php">Log in</a> </p>
                </form>
            </div>
        </div>
</body>
</html>
