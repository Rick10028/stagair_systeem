<?php
require_once 'controllers/authControllers.php';
?>
<?php
require_once 'includes/header.php';
?>

    <body>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6 left-panel">
                <div class="overlay">
                </div>
            </div>
            <div class="col-sm-6 login">
                <div class="col-md-6 offset-md-3 mt-5 login-form" form-div login>
                    <form action="login.php" method="post">
                        <h3 class="text-center">Log in</h3>
                        <?php if(count($errors) > 0): ?>
                            <div class="alert alert-danger">
                                <?php foreach($errors as $error): ?>
                                    <li>
                                        <?php echo $error; ?>
                                    </li>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label for="username">Email</label>
                            <input type="text" name="username" value="" class="form-control form conrtol-lg">
                        </div>
                        <div class="form-group">
                            <label for="password">Wachtwoord</label>
                            <input type="password" name="password" class="form-control form conrtol-lg">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="login-btn" class="btn btn-primary btn-block btn-lg">Login</button>
                        </div>
                        <p class="text-center">Nog geen account? <a href="registreer.php">Registreer!</a> </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </body>

    </html>
